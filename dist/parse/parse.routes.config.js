"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParseRoutes = void 0;
const common_routes_config_1 = require("../common/common.routes.config");
class ParseRoutes extends common_routes_config_1.CommonRoutesConfig {
    constructor(app) {
        super(app, 'ParseRoutes');
    }
    isString(x) {
        return Object.prototype.toString.call(x) === "[object String]";
    }
    onlyLetters(x) {
        let regExp = /[^a-z]/g;
        return x.toLowerCase().replace(regExp, '');
    }
    configureRoutes() {
        this.app.route(`/api/v1/parse`)
            .all((req, res, next) => {
            next();
        })
            .post((req, res) => {
            const data = req.body.data;
            if (this.isString(data)) {
                const clientId = data.slice(18, 25);
                const firstName = data.slice(0, 8);
                const lastName = data.slice(8, 18);
                const result = {
                    statusCode: 200,
                    data: {
                        firstName,
                        lastName,
                        clientId
                    }
                };
                res.status(200).send(result);
            }
            else {
                res.status(500).send('error in data');
            }
        });
        this.app.route(`/api/v2/parse`)
            .all((req, res, next) => {
            next();
        })
            .post((req, res) => {
            const data = req.body.data;
            if (this.isString(data)) {
                const clientId = `${data.slice(18, 21)}-${data.slice(21, 25)}`;
                const firstName = data.split(0)[0];
                const lastName = data.split(0).filter((v) => v != '')[1];
                const result = {
                    statusCode: 200,
                    data: {
                        firstName,
                        lastName,
                        clientId
                    }
                };
                res.status(200).send(result);
            }
            else {
                res.status(500).send('error in data');
            }
        });
        return this.app;
    }
}
exports.ParseRoutes = ParseRoutes;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyc2Uucm91dGVzLmNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhcnNlL3BhcnNlLnJvdXRlcy5jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEseUVBQWtFO0FBR2xFLE1BQWEsV0FBWSxTQUFRLHlDQUFrQjtJQUMvQyxZQUFZLEdBQXdCO1FBQ2hDLEtBQUssQ0FBQyxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUNELFFBQVEsQ0FBQyxDQUFTO1FBQ2QsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssaUJBQWlCLENBQUE7SUFDbEUsQ0FBQztJQUNELFdBQVcsQ0FBQyxDQUFTO1FBQ2pCLElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQztRQUN2QixPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDO2FBQzFCLEdBQUcsQ0FBQyxDQUFDLEdBQW9CLEVBQUUsR0FBcUIsRUFBRSxJQUEwQixFQUFFLEVBQUU7WUFDN0UsSUFBSSxFQUFFLENBQUM7UUFDWCxDQUFDLENBQUM7YUFDRCxJQUFJLENBQUMsQ0FBQyxHQUFvQixFQUFFLEdBQXFCLEVBQUUsRUFBRTtZQUNsRCxNQUFNLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMzQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3JCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFBO2dCQUNuQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQTtnQkFDakMsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sTUFBTSxHQUFHO29CQUNYLFVBQVUsRUFBRSxHQUFHO29CQUNmLElBQUksRUFBRTt3QkFDRixTQUFTO3dCQUNULFFBQVE7d0JBQ1IsUUFBUTtxQkFDWDtpQkFDSixDQUFBO2dCQUNELEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2hDO2lCQUFNO2dCQUNILEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3pDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUM7YUFDMUIsR0FBRyxDQUFDLENBQUMsR0FBb0IsRUFBRSxHQUFxQixFQUFFLElBQTBCLEVBQUUsRUFBRTtZQUM3RSxJQUFJLEVBQUUsQ0FBQztRQUNYLENBQUMsQ0FBQzthQUNELElBQUksQ0FBQyxDQUFDLEdBQW9CLEVBQUUsR0FBcUIsRUFBRSxFQUFFO1lBQ2xELE1BQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzNCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDckIsTUFBTSxRQUFRLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFBO2dCQUM5RCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUNsQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQVMsRUFBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMvRCxNQUFNLE1BQU0sR0FBRztvQkFDWCxVQUFVLEVBQUUsR0FBRztvQkFDZixJQUFJLEVBQUU7d0JBQ0YsU0FBUzt3QkFDVCxRQUFRO3dCQUNSLFFBQVE7cUJBQ1g7aUJBQ0osQ0FBQTtnQkFDRCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNoQztpQkFBTTtnQkFDSCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUN6QztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7Q0FDSjtBQTVERCxrQ0E0REMifQ==
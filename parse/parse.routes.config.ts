import {CommonRoutesConfig} from '../common/common.routes.config';
import express from 'express';

export class ParseRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'ParseRoutes');
    }
    isString(x: string) {
        return Object.prototype.toString.call(x) === "[object String]"
    }
    onlyLetters(x: string) {
        let regExp = /[^a-z]/g;
        return x.toLowerCase().replace(regExp, '');
    }
    configureRoutes() {
        this.app.route(`/api/v1/parse`)
            .all((req: express.Request, res: express.Response, next: express.NextFunction) => {
                next();
            })
            .post((req: express.Request, res: express.Response) => {
                let result;
                const data = req.body.data;
                if (this.isString(data)) {
                    const clientId = data.slice(18, 25)
                    const firstName = data.slice(0,8)
                    const lastName = data.slice(8, 18);
                    result = {
                        statusCode: 200,
                        data: {
                            firstName,
                            lastName,
                            clientId
                        }
                    }
                    res.status(200).send(result);
                } else {
                    res.status(500).send({
                        statusCode: 500
                    });
                }
            });
        this.app.route(`/api/v2/parse`)
            .all((req: express.Request, res: express.Response, next: express.NextFunction) => {
                next();
            })
            .post((req: express.Request, res: express.Response) => {
                const data = req.body.data;
                if (this.isString(data)) {
                    const clientId = `${data.slice(18, 21)}-${data.slice(21, 25)}`
                    const firstName = data.split(0)[0]
                    const lastName = data.split(0).filter((v: string)=> v != '')[1]
                    const result = {
                        statusCode: 200,
                        data: {
                            firstName,
                            lastName,
                            clientId
                        }
                    }
                    res.status(200).send(result);
                } else {
                    res.status(500).send('error in data');
                }
            });
        return this.app;
    }
}
